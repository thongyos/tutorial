#!/usr/bin/python

import serial
import socket
import time
import datetime
import constants
import hashlib
import taikoFunction as taiko
from subprocess import call
from binascii import unhexlify

serT = serial.Serial(
        "/dev/ttyAMA0",
        baudrate=9600,
        bytesize=serial.EIGHTBITS,
        parity=serial.PARITY_EVEN,
        stopbits=serial.STOPBITS_ONE,
        timeout = 1
        )
######### Now reset Taiko bill acceptor at first time(just power-up taiko) ######

serT.flushInput()
serT.write(unhexlify(constants.TAIKO_STAT_REQ))
reader = serT.read(5).encode("hex")
if reader != constants.TAIKO_IDLING:
	taiko.resetTaiko(serT)
time.sleep(0.2)


######### fetch table Msg ####################

def fetch_tablet_msg(msg):
    msgSize = len(msg)
    if msgSize >= 10:
        stx = msg[0:2]
        print "stx => ", stx
        size = msg[2:4]
        print "size => ", size
        msgType = msg[4:6]
        print "type => ", msgType
	    headerID = msg[6:10]
	    print "headerID => ", headerID
        if stx == "41":
            if msgType == "01":
                if headerID == "4100":
                    taiko.resetTaiko(serT)
                    print "Now we are sending Reset command to taiko"
                elif headerID == "4102":
                    data = msg[10:14]
                    selectedCol = "0a0000002608420001e7e7" + dc.dataConv[data[2:4]]
                    selectedColMsg = constants.VM_STX + vm.escapeMsg(constants.VM_AUTH_MSG)
                elif headerID == "4105":
                    which = msg[10:12]
                    numFile = msg[12:14]
                    if which == "00":
                        updateFirmware("bill", int(numFile, 16))
                    elif which == "01":
                        updateFirmware("coin", int(numFile, 16))
                    elif which == "02":
                        updateFirmware("pi", int(numFile, 16))
            elif msgType == "02":
                pass
            elif msgType == "04":
                data = msg[10:14]
                print "data => ", data
                if headerID == "4101":
                    if data == "0000":
                        print "Set Inhibit 0"
                        taiko.setInhibit(serT, 0)
                    elif data == "0001":
                        print "Set Inhibit 1"
                        taiko.setInhibit(serT, 1)
                        
# Connect to Tablet via USBTethering ################
clientA = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientA.connect(('192.168.42.129', 8080))
clientA.settimeout(2.0)

######### Main loop ##########
while True:
    
    moneyMsg = "41050041ff"
    correctMoney = False
## get input from taiko(insert bank)
    serT.flushInput()
    serT.write(unhexlify(constants.TAIKO_STAT_REQ))
    response = serT.read(5)
    decode = response.encode("hex")
    time.sleep(0.2)
    if decode[4:6] == "13":
        bank = decode[6:8]
	serT.write(unhexlify(constants.TAIKO_STACK1))
    if decode[4:6] == "15":
        serT.write(unhexlify(constants.TAIKO_ACK))
        if bank == "62":
                moneyMsg = moneyMsg + "0005"
                correctMoney = True
                result = "20 Baht"
        elif bank == "63":
                moneyMsg = moneyMsg + "0006"
                correctMoney = True
                result = "50 Baht"
        elif bank == "64":
                moneyMsg = moneyMsg + "0007"
                correctMoney = True
                result = "100 Baht"
        elif bank == "66":
                moneyMsg = moneyMsg + "0008"
                correctMoney = True
                result = "500 Baht"
        elif bank == "67":
                moneyMsg = moneyMsg + "0009"
                correctMoney = True
                result = "1000 Baht"
        print result
    if correctMoney:# send to tablet
        clientA.send(unhexlify(moneyMsg))
    try:
        global readerA
        readerA = clientA.recv(16)
    except socket.timeout:
        pass
    print "Msg receive from tablet: " + readerA.encode('hex')
    fetch_tablet_msg(readerA)