#!/usr/bin/python
import serial
import socket
import time
import datetime
import hashlib
from subprocess import call
from binascii import unhexlify

ser = serial.Serial("/dev/ttyUSB0",19200,rtscts=0,dsrdtr=0,timeout=1.0)

########## Establish ppp connection ###########
global readerA
readerA = ''
global data
data = ''
while 1:
    ser.flushInput()
    serial_read = ser.read(2)
    print serial_read
    if serial_read == "AT":
        print "AT found." 
        break

ser.write('CLIENT')
while 1:
    ser.flushInput()
    reader = ser.read(12)
    print reader
    if reader == 'CLIENTSERVER': 
        print 'CLIENTSERVER found.'
        break
    else :
        print 'CLIENT send again.'
        ser.write('CLIENT')

print 'Succeed'
call(["sudo","pppd","-d","/dev/ttyUSB0","19200"])
time.sleep(5.0)
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(('192.168.244.3', 20000))
client.settimeout(2.0)
######## Finish establish ppp connection ##############

statReq = "\x07\x00\x00\x31\x65\x01\x00\x00"

#2 authStr = 12645 authentication

authStr = "\x02\x7d\x33\x7d\x20\x7d\x20\x7d\x20\x7d\x1f\x7d\x4b\x7d\x62\x7d\x20\x7d\x22\x7d\xc7\x7d\xc2\x7d\x83\x7d\xf3\x7d\x9e\x7d\x81\x7d\xc7\x7d\xc2\x7d\xc6\x7d\xc6\x7d\xc6\x7d\xc6"

authEvent = "020700ffff4f68000000" #1 event VM send first to us 16742,000

authConfirm = "\x02\x07\x00\x00\x00\x3f\x0f\x01\x00\x00" #3 send for confirm authen 

aqVMState = "\x02\x07\x00\x00\x00\x4f\x04\x01\x00\x00"

aqClock = "\x02\x07\x00\x00\x00\x5d\x0f\x01\x00\x00"

selectedCol = "\x02\x7d\x2a\x7d\x20\x7d\x20\x7d\x20\x7d\x06\x7d\x28\x7d\x62\x7d\x20\x7d\x21\x7d\xc7\x7d\xc7\x7d\xca"

def authStrfunc()
    client.send(authStr) 
    print "Push data: ", authStr.encode('hex')

def authConfirmfunc
    client.send(authConfirm)
    print "Get data: ", authConfirm.encode('hex')
    

######## Making authentication with VM ################


while 1:
	try:
        	global data
        	data = client.recv(256)
    	except socket.timeout:
        	pass
	if data:
            datahex = data.encode('hex')
        	print "socket hex: ", datahex
            if datahex == authEvent: # check if = 16742,000
                print "found authEvent:"
                authStrfunc() # go for push authen
            elif data.encode('hex')[10:14] == "3f6b": # check if VM reply with 0000
                if data.encode('hex')[-4:] == "0000":
                    print "found reply authen"
                    authConfirmfunc() # go for authenConfirm
            elif data.encode('hex')[10:14] == "3f0f": # check authen confirm or not
                if data.encode('hex')[-2:] == "e7":
                    print "authen success"
                elif data.encode('hex') == "e6":
                    print "authen NOT success"
            else:
            print "socket hex didn't match any if condition", datahex
	data = ''