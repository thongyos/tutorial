import constants
import time
from binascii import unhexlify

def resetTaiko(serT):
        #print "Resetting Taiko..."
        #isPowerup = False
        #while not isPowerup:
        #       ser.write(statReq)
        #       reader = ser.read(5).encode("hex")
        #       if reader == powerup:
        #               isPowerup = True
        #       time.sleep(0.2)
        #print "Taiko powered up."
        serT.write(unhexlify(constants.TAIKO_RESET))
        print "Resetting..."
        isAck = False
        while not isAck:
                serT.flushInput()
                reader = serT.read(5).encode("hex")
                if reader == constants.TAIKO_ACKRECV:
                        isAck = True
                time.sleep(0.2)
        print "Ack Received."
        isInit = False
        while not isInit:
                serT.flushInput()
                serT.write(unhexlify(constants.TAIKO_STAT_REQ))
                reader = serT.read(5).encode("hex")
                if reader == constants.TAIKO_INITIAL:
                        isInit = True
                time.sleep(0.2)
        print "Initializing..."
        isTinhibit = False
	while not isTinhibit:
                serT.flushInput()
                serT.write(unhexlify(constants.TAIKO_STAT_REQ))
                reader = serT.read(5).encode("hex")
                if reader == constants.TAIKO_TINHIBIT:
                        isTinhibit = True
                time.sleep(0.2)
        print "Ready to inhibit."
        serT.write(unhexlify(constants.TAIKO_INHIBIT0))
        print "Set inhibit."
        isIdling = False
        while not isIdling:
                serT.flushInput()
                serT.write(unhexlify(constants.TAIKO_STAT_REQ))
                reader = serT.read(5).encode("hex")
                if reader == constants.TAIKO_IDLING:
                        isIdling = True
                time.sleep(0.2)
        print "Inhibited."


def setInhibit(ser, val):
        if val:
                ser.write(unhexlify(constants.TAIKO_INHIBIT1))
        else :
                ser.write(unhexlify(constants.TAIKO_INHIBIT0))
                
